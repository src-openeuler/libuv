Name:           libuv
Epoch:          1
Version:        1.47.0
Release:        2
Summary:        A multi-platform support library with a focus on asynchronous I/O

# from README.md
License:        MIT and CC-BY-4.0
URL:            http://libuv.org/
Source0:        http://dist.libuv.org/dist/v%{version}/%{name}-v%{version}.tar.gz
Source2:        %{name}.pc.in
Source3:        libuv.abignore

# Test fix for IPv6 interfaces with a NULL ifa_addr
# https://github.com/libuv/libuv/pull/4218
Patch1: 0001-unix-ignore-ifaddrs-with-NULL-ifa_addr-4218.patch

# test: check if ipv6 link-local traffic is routable
# https://github.com/libuv/libuv/pull/4220
Patch2: 0002-test-check-if-ipv6-link-local-traffic-is-routable.patch

# test: Use unsigned comparison for fs_type
# https://github.com/libuv/libuv/pull/4227
Patch3: 0003-test_fs.c-Fix-issue-on-32-bit-systems-using-btrfs.patch

Patch6000: backport-0001-CVE-2024-24806.patch
Patch6001: backport-0002-CVE-2024-24806.patch
Patch6002: backport-0003-CVE-2024-24806.patch

BuildRequires:  autoconf automake libtool gcc make

%description
libuv is a multi-platform support library with a focus on asynchronous I/O. 
It was primarily developed for use by Node.js, but it’s also used by Luvit,
Julia, pyuv, and others.

%package devel
Summary:        Development libraries for libuv
Requires:       %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Obsoletes:      %{name}-static < %{version}-%{release}
Provides:       %{name}-static

%description devel
Development libraries for libuv

%package_help

%prep
%autosetup -p1 -n %{name}-v%{version}

%build
./autogen.sh
%configure --disable-silent-rules
%make_build

%install
%make_install
%delete_la
mkdir -p %{buildroot}%{_libdir}/libuv/
install -Dm0644 -t %{buildroot}%{_libdir}/libuv/ %{SOURCE3}

%ldconfig_scriptlets

%check
make check

%files
%license LICENSE
%{_libdir}/%{name}.so.*
%{_libdir}/libuv/libuv.abignore

%files devel
%{_libdir}/%{name}.so
%{_libdir}/%{name}.a
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/uv.h
%{_includedir}/uv/

%files help
%doc README.md AUTHORS CONTRIBUTING.md MAINTAINERS.md SUPPORTED_PLATFORMS.md
%doc ChangeLog

%changelog
* Sun Feb 18 2024 shixuantong <shixuantong1@huawei.com> - 1:1.47.0-2
- fix CVE-2024-24806

* Mon Nov 27 2023 Jingwiw  <wangjingwei@iscas.ac.cn> - 1:1.47.0-1
- Upgrade to 1.47.0

* Thu May 04 2023 lilong <lilong@kylinos.cn> - 1:1.44.2-1
- Upgrade to 1.44.2

* Mon Apr 24 2023 shixuantong <shixuantong1@huawei.com> - 1:1.42.0-5
- fix Obsoletes in spec and remove ldconfig_scriptlets from check

* Thu Dec 15 2022 shixuantong <shixuantong1@huawei.com> - 1:1.42.0-4
- add make to buildrequires

* Thu Dec 08 2022 shixuantong <shixuantong1@huawei.com> - 1:1.42.0-3
- skip some test

* Mon Jun 20 2022 renhongxun <renhongxun@h-partners.com> - 1.42.0-2
- enable check

* Sat Sep 25 2021 sdlzx <hdu_sdlzx@163.com> - 1.42.0-1
- upgrade version to 1.42.0

* Tue Jan 26 2021 liudabo <liudabo1@huawei.com> - 1.40.0-1
- upgrade version to 1.40.0

* Mon Dec 14 2020 wangxiao <wangxiao65@huawei.com> - 1.38.1-2
- fix CVE-2020-8252

* Mon Jul 27 2020 wenzhanli <wenzhanli2@huawei.com> - 1.38.1-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:version update 1.38.1

* Mon Jun 1 2020 lizhenhua <lizhenhua21@huawei.com> - 1.35.0-1
- update to 1.35.0

* Tue Dec 3 2019 mengxian <mengxian@huawei.com> - 1.23.0-2
- Package init
